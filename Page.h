#ifndef PAGE_H
#define PAGE_H
#include <iostream>
#include <vector>

using namespace std;

void insertarEn(int *array, int pos, int size, int agregar)
{
    int arrayTemp[size];
    memcpy(arrayTemp, array, sizeof(int)*(pos));
    arrayTemp[pos-1]=agregar;
    memcpy(arrayTemp+pos, array+pos-1, sizeof(int)*(size-(pos)));
    memcpy(array, arrayTemp, sizeof(int)*size);
}

template<class T>
void insertarSons(T* *array, int pos, int size, T *agregar)
{
    T* arrayTemp[size];
    memcpy(arrayTemp, array, sizeof(T*)*(pos));
    arrayTemp[pos-1]=agregar;
    memcpy(arrayTemp+pos, array+pos-1, sizeof(T*)*(size-(pos)));
    memcpy(array, arrayTemp, sizeof(T*)*size);
}

template <class T>
class Page
{
    public:
        long * fileAdress;
        int orden;
        int usados;
        int maxDegree;
        Page<T> * Dad;
        Page<T> ** Son;
        T * Datas;

        void insertData2(T data)
        {
            int whereInsert=0;
            if(!findKey(data,whereInsert))
            {
                insertarEn(this->Datas,whereInsert,orden,data);
                usados++;

            }
        }

        int insertData(T data)
        {
            int whereInsert=0;
            if(!findKey(data,whereInsert))
            {
                insertarEn(this->Datas,whereInsert,orden,data);
                usados++;

            }
            return whereInsert;
        }
        Page(int orden)
        {
            this->usados = 0;
            this->orden = orden;
            this->maxDegree=orden+1;
            Datas = new T[orden+1];
            Son = new Page<T> * [orden+2];
        }
        bool isFull()
        {
            return usados==orden;
        }

        bool findKey(T DataToSearch, int & pointer)
        {
            int inicioL=0;
            int finalL=usados-1;
            int medio=0;

            while(inicioL<=finalL)
            {
                medio=(inicioL+finalL)/2;
                if(Datas[medio]==DataToSearch) return true;
                if(Datas[inicioL]==DataToSearch) return true;
                if(Datas[finalL]==DataToSearch) return true;

                //cout<<"medio->"<<medio<<endl;
                //cout<<inicioL<<"("<<Datas[inicioL]<<")"<<"-"<<finalL<<"("<<Datas[finalL]<<")"<<endl;

                if(DataToSearch>Datas[medio]) inicioL = medio+1;
                else finalL = medio-1;
            }

            if(Datas[medio]>DataToSearch) pointer=medio;
            else if(Datas[usados-1]<DataToSearch) pointer=medio+1;
            else pointer = medio+1;
            //cout<<"->"<<pointer<<endl;
            return false;

        }



    protected:
    private:
};

#endif // PAGE_H

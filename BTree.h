#ifndef BTREE_H
#define BTREE_H

#include "Page.h"

void insertarEn(int *array, int pos, int size, int agregar);
int insertarFull(int *&array,int pos, int size, int agregar, int *&,int*&);

template <class T>
class BTree
{
    public:
        int orden;
        Page<T> * root;

        BTree(int orden)
        {
            this->orden = orden;
            this->root = 0;
        }
    /////faltaaaaaaaaaa
    void insertar(T valor)
    {
        if(!root) {root = new Page<T> (orden); root->Datas[0]=valor; root->usados++;}
        else
        {
            Page<T> * aux= encontrar(valor,root);
            insertar(valor,aux);
        }
    }
    void insertar(T valor, Page<T> * aux )
    {
        if(!aux) return;
        int whereInsert=0;
        if(aux->findKey(valor,whereInsert)) return;

        if(aux->isFull())
        {
            Page<T> * nuevo = new Page <T>(orden);

            if(!aux->Dad)
            {
                Page<T> * newDad = new Page <T>(orden);
                T * nuevoArray = new T[orden];
                aux->Dad=newDad;
                nuevo->Dad=newDad;
                newDad->insertData(insertarFull(aux->Datas,whereInsert,orden,valor,nuevoArray,nuevo->Datas));
                newDad->Son[0]=aux;
                newDad->Son[1]=nuevo;
                delete aux->Datas;
                aux->Datas=nuevoArray;
            }
            else
            {
                int whereInsertDad =0;
                Page<T> * newDad = aux->Dad;
                //newDad->findKey();
                T * nuevoArray = new T[orden];
                nuevo->Dad=newDad;
                int aux2 = newDad->insertData(insertarFull(aux->Datas,whereInsert,orden,valor,nuevoArray,nuevo->Datas));
                newDad->Son[aux2]=aux;
                newDad->Son[aux2+1]=nuevo;
                delete aux->Datas;
                aux->Datas=nuevoArray;
            }

        }

        else
        {
            insertarEn(aux->Datas,whereInsert+1,orden,valor);
            aux->usados++;
        }


    }
    Page<T> * encontrar(T valor, Page<T> * aux)
    {
        int aux2=0;
        if(!aux->findKey(valor,aux2))
        {
            if(aux->Son[aux2]==0)
            {
                return aux;
            }
            else
            {
                return encontrar(valor,aux->Son[aux2]);
            }
        }
        else
        {
            return 0;
        }
    }

    void imprimiendo(Page<T> *testing,int size)
    {
        if(!testing) return;
        for(auto i=0; i<size;i++) cout<<testing->Datas[i]<<' ';
        cout<<'\n';
        for(auto i=0; i<size;i++) imprimiendo(testing->Son[i],size);
    }
};


//no full
/*void insertarEn(int *array, int pos, int size, int agregar)
{
    //rellenar con ceros seria mejor para evitar ambigüedad
    int arrayTemp[size];
    memcpy(arrayTemp, array, sizeof(int)*(pos));
    arrayTemp[pos-1]=agregar;
    memcpy(arrayTemp+pos, array+pos-1, sizeof(int)*(size-(pos)));
    memcpy(array, arrayTemp, sizeof(int)*size);
}*/

int insertarFull(int *&array,int pos, int size, int agregar, int *&arraypart1, int *&arraypart2)
{
    int arrayTemp[size+1];
    memcpy(arrayTemp, array, sizeof(int)*(pos));
    arrayTemp[pos-1]=agregar;
    memcpy(arrayTemp+pos, array+pos-1, sizeof(int)*(size-(pos-1)));
    //memcpy(array, arrayTemp, sizeof(int)*size);
    //imprimiendo(arrayTemp,size+1);
    memcpy(arraypart1,arrayTemp,sizeof(int)*(size/2));
    memcpy(arraypart2,arrayTemp+size/2+1,sizeof(int)*size/2);
    return arrayTemp[size/2];
}


#endif // BTREE_H
